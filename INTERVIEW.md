# Vessel attribute service

## Exercise description
We have fleets of vessels which transport LNG from one port to another. These vessels have certain characteristics such as speed, fuel consumption, and LNG carrying capacity. Your task is to build a small web app that allows a user to select a vessel from a list and display/edit its attributes.

## Architecture

### Backend
Create a backend service with any endpoints that you deem necessary. For example we could have the following endpoints:

- GET http://localhost/vessel/{id}
- GET http://localhost/vessels
- POST http://localhost/vessel

The JSON representation of a vessel should have the following attributes:

    {
      id: NUMBER,
      name: STRING,
      capacity: NUMBER,
      speed: NUMBER,
      fuelConsumption: NUMBER,
      version: NUMBER,
      notes: STRING,
    }

Here is a list of constraints in the modeling of the vessel service:

- `id` must be unique
- `fuelConsumption` must depend on the speed and the capacity
- `version` of the vessel must start at 0 and be incremented with every change to an attribute

### Frontend
The API design is more important to us then the frontend. Feel free to make this is as simple/complex as you wish.

### Optional
Add JUnit5 tests for the vessel service

