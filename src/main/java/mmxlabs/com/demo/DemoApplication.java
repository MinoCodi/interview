package mmxlabs.com.demo;

import mmxlabs.com.demo.Vessel;
import mmxlabs.com.demo.VesselRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private VesselRepository vesselRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		vesselRepository.deleteAll();

		// save some vessels
		vesselRepository.save(new Vessel("smallCarrier", 6.6f, 10.4f, 1, "fast and small ship"));
		vesselRepository.save(new Vessel("bigCarrier", 3.2f, 30.7f, 1, "fast and small ship"));

		// fetch all vessels
		System.out.println("Vessels found with findAll(): ");
		for (Vessel vessel : vesselRepository.findAll()) {
			System.out.println(vessel);
		}
	}

}
