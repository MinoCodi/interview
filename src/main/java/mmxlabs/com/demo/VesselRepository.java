package mmxlabs.com.demo;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface VesselRepository extends MongoRepository<Vessel, String> {

    public Vessel findByName(String name);
    public List<Vessel> findBySpeed(int speed);
}