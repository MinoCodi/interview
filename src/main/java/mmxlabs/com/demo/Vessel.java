package mmxlabs.com.demo;

import org.springframework.data.annotation.Id;

public class Vessel {

    @Id
    public String id;

    public String name;
    public float speed;
    public float fuelConsumption;
    public int version;
    public String notes;

    public Vessel() {}

    public Vessel(
        String name,
        float speed,
        float fuelConsumption,
        int version,
        String notes)
    {
        this.name = name;
        this.speed = speed;
        this.fuelConsumption = fuelConsumption;
        this.version = version;
        this.notes = notes;
    }

    @Override
    public String toString() {
        return String.format(
            "Vessel[name=%s, speed=%f, fuelConsumption=%f, version=%d, notes=%s",
            name, speed, fuelConsumption, version, notes);
    }

}