# Interview demo

Starter project Minimax Labs

## Requirements
* Java 1.8
* Maven
* NodeJS
* MongoDB

## Getting started
Clone the project by running `git clone https://gitlab.com/philippemmx/interview.git` then run `mvn clean install` to install the dependencies.

## Workflow
You can compile the project by running `mvn package` which will create a jar file in the target folder. You can run this using `java -jar target\demo-0.0.1-SNAPSHOT.jar`. You should see the default angular setup page at localhost:9000/index.html

Since the app takes a while to package, we recomend running the front & backend separately during development. Angular is run with `ng serve` and spring boot is run with `mvn spring-boot:run`. The frontend will be available at localhost:4200 and the backend is at localhost:9000

### Mongo setup
Create a mongo instance with a "vessels" database then create a user for the database using `db.createUser({user:"USERNAME", pwd:"PASSWORD", roles:["dbOwner"]});` and put the USERNAME & PASSWORD into the application.properties file of src/main/resources.

## Resources
* Front end: https://angular.io/
* UI components library: https://www.primefaces.org/primeng/#/
* Back end: https://spring.io/projects/spring-boot
* Database: https://www.mongodb.com/
* Tutorial: https://www.baeldung.com/spring-boot-angular-web
* Maven: https://maven.apache.org/
* IDE (Eclipse): https://www.eclipse.org/